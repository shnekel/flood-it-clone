var webpack = require('webpack');
var path = require('path');
var loaders = require('./webpack.loaders');

var BUILD_DIR = path.resolve(__dirname, "");
var APP_DIR = path.resolve(__dirname, "src");

module.exports = {
	entry: {
        app: process.env.NODE_ENV === 'production' ? [APP_DIR + "/app.js"] : ['webpack/hot/dev-server', APP_DIR + "/app.js"]
	},
    output: {
        path: BUILD_DIR,
        filename: 'bundle.js'
    },
    module: {
        loaders: loaders
    },
	resolve: {
		extensions: ['', '.js', '.jsx']
	},
    plugins: [
		new webpack.NoErrorsPlugin()
	]
};
