'use strict';

const COLORS = [
    'red',
    'green',
    'blue',
    'yellow',
    'cyan',
    'magenta'
];

const BOARD_SIZE = 10;

class App {
    constructor(parent) {
        this.el = document.createElement('div');
        this.parent = document.getElementById(parent);
        this.gameBoard = this.initGameBoard();
        this.startColor = this.gameBoard[0][0];
        this.steps = 0;
        
        var buttons = document.getElementsByTagName('button');
        for (var button of buttons) {
            button.addEventListener('click', this.fillAreaWrapped.bind(this), false);
        }

        var restart = document.getElementById('restart');
        restart.addEventListener('click', function() {
            this.gameBoard = this.initGameBoard();
            this.startColor = this.gameBoard[0][0];
            this.steps = 0;
            this.refillBoard();
            this.addStep();
        }.bind(this), false);
    }

    getIndexByColor(color) {
        return COLORS.indexOf(color);
    }
    
    fillAreaWrapped(e) {
        this.oldColor = this.startColor;
        this.startColor = this.getIndexByColor(e.target.className);
        this.fillArea(0, 0, this.oldColor, this.startColor);
        this.refillBoard();
        this.addStep();
        if (!this.isWinCondition()) {
            document.getElementById('steps').innerHTML += ' YOU WIN!!!!';
        }
    } 
    
    addStep() {
        document.getElementById('steps').innerHTML = ++this.steps;
    }

    isWinCondition() {
        for (var i=0; i<BOARD_SIZE; i++) {
            for (var j=0; j<BOARD_SIZE; j++) {
                if (this.gameBoard[j][i] !== this.startColor) {
                    return true;
                }
            }
        }
        return false;
    }
    
    refillBoard() {
        for (var i=0; i<BOARD_SIZE; i++) {
            for (var j=0; j<BOARD_SIZE; j++) {
                var el = document.querySelectorAll('[data-position="' + j + "_" + i +'"]');
                if (el[0]) {
                    el[0].className = COLORS[this.gameBoard[j][i]];
                }
            }
        }
    }
    
    fillArea(node_x, node_y, target, replacement) {
        if (node_y < 0 || node_y > BOARD_SIZE - 1) {
            return false;
        }

        if (node_x < 0 || node_x > BOARD_SIZE - 1) {
            return false;
        }

        var node = this.gameBoard[node_y][node_x];
        if (node == replacement) {
            return false;
        }

        if (node !== target) {
            return false;
        }

        this.gameBoard[node_y][node_x] = replacement;
        this.fillArea(node_x+1, node_y, target, replacement);
        this.fillArea(node_x-1, node_y, target, replacement);
        this.fillArea(node_x, node_y+1, target, replacement);
        this.fillArea(node_x, node_y-1, target, replacement);
    }
    
    initGameBoard() {
        var board = [];
        for (var i=0; i<BOARD_SIZE; i++) {
            var line = [];
            for (var j=0; j<BOARD_SIZE; j++) {
                line.push(this.setRandomColorIndex(COLORS));
            }
            board.push(line);
        }
        return board;
    }

    setRandomColorIndex(list) {
        return Math.floor(Math.random() * list.length);
    }
    
    getColorByIndex(index) {
        return COLORS[index];
    }
    
    renderTable() {
        var board = document.createElement('table');
        
        for (var i=0; i<BOARD_SIZE; i++) {
            var row = document.createElement('tr');
            for (var j=0; j<BOARD_SIZE; j++) {
                var col = document.createElement('td');
                col.className = this.getColorByIndex(this.gameBoard[j][i]);
                col.setAttribute('data-position', j + '_' + i);
                row.appendChild(col);
            }
            board.appendChild(row);
        }
        return board;
    }
    
    run() {
        this.el.appendChild(this.renderTable());
        this.parent.appendChild(this.el);
    }
}

(new App('app')).run();



